<html>
<head>
<title>Display list</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script> --}}
</head>

<body>
<div class="card mb-3">
  </i> List of Users
                
    </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th><b> id</b></th>
                                <th><b> Username</b></th>
                                <th><b>Password</b></th>
                              
                             </tr>
                        </thead>


                        <tbody>
                            @foreach ($students as $student)
                                <tr>
                                     <td>{{$student->id}}</td>
                                      
                                    <td>{{$student->username}}</td>
                                    <td>{{$student->password}}</td>
                                   
                                    <td><a href="{{route('students.show',$student->id)}}">show </a>    <a  href="{{route('students.edit',$student->id)}}">edit</a> </td> 
                                    <td>
                                    <form action="{{route('students.destroy',$student->id)}}" method="POST">
                                    @csrf 
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger" type="submit">Delete</button>
                                    </form>
                                    </td>
                                    </tr>       
                             @endforeach

                        </tbody>
                    </table>
                </div>
            </div> 
</div>            
</body>
</html>