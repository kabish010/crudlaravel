<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\student;

class studentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students= student::orderBy('id','asc')->get();
 
        return view('students.index',compact('students'));
 
        //
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username'=>'required',
            'password'=>'required',
          
        ]);
 
        $student= new student([
         'username' => $request->get('username'),
        'password' => $request->get('password'),
        
        ]);
 
        $student->save();
 
         return redirect('/students')->with('success','New user has been added');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $posts = Post::orderBy('id', 'DESC')->get();
        $student = student::find($id);
                   
        return view('students.show', compact('student'));
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student= student::find($id);

        return view('students.edit', compact('student'));
       
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update (Request $request, $id)
    {
        $request->validate([
            'username'=>'required',
            'password'=>'required',
            
        ]);
 
        $student = student::find($id);
        $student->username = $request->get('username');
        $student->password  = $request->get('password');
       
 
        $student->save();
 
        return redirect('/students')->with('success','User has been updated');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $student = student::find($id);
        $student->delete();
   
        return redirect('/students')->with('success', 'User has been deleted Successfully');
        //
       
    }
}
